ZGEN_SYNTAX_HIGHLIGHTING="yes"
ZGEN_AUTO_SUGGESIONS="yes"

# select your prompt, if you have powerline patched fonts installed,
# and selected as terminal font, agnoster is a good option
zgen prezto prompt theme 'pure'
